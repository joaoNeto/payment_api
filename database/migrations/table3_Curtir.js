'use strict'

const Schema = use('Schema')

class RatePostSchema extends Schema {
  up () {
    this.create('ratePost', (table) => {
      table.integer('id_user').notNullable().unsigned().references('id').inTable('user')
      table.integer('id_post').notNullable().unsigned().references('id').inTable('post')
      table.enu('rate', ['like', 'deslike'])
      table.timestamps()
    })
  }

  down () {
    this.dropIfExists('ratePost')
  }
}

module.exports = RatePostSchema
