'use strict'

const Schema = use('Schema')

class FriendSchema extends Schema {
  up () {
    this.create('friend', (table) => {
      table.integer('id_user_from').notNullable().unsigned().references('id').inTable('user')
      table.integer('id_user_to').notNullable().unsigned().references('id').inTable('user')
      table.timestamps()
    })
  }

  down () {
    this.dropIfExists('friend')
  }
}

module.exports = FriendSchema