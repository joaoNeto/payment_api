'use strict'

const Schema = use('Schema')

class PostSchema extends Schema {
  up () {
    this.create('post', (table) => {
      table.increments('id')
      table.integer('id_user').notNullable().unsigned().references('id').inTable('user')
      table.string('text', 150).notNullable()
      table.string('attachment', 100)
      table.timestamps()
    })
  }

  down () {
    this.dropIfExists('post')
  }
}

module.exports = PostSchema
