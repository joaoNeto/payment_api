if(localStorage.getItem("tokenAcess")){
	window.location = "/dashboard";
}


$('.message a').click(function(){
   $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
});

$("#createAccount").click(function (){
	let data = {};
	$(this).html("Checking...");

	$(".register-form").serializeArray().forEach((input) => {
		data[input.name] = input.value;
	});

	$.ajax({
		url: "/user",
		data: data,
		type: "POST", 
		success: function(response, status){
			$("#createAccount").html("CREATE");
			if(response.data.token){
				localStorage.setItem("tokenAcess", response.data.token);
				localStorage.setItem("nameUser", data.username);
				window.location = "/dashboard";
			}else{
				$(".notificacao").html("Erro ao fazer cadastrar!").fadeIn();
				setTimeout(function(){ $(".notificacao").fadeOut(); }, 2000);
			}
		},
		error: function (response, status, error) {
			$("#createAccount").html("CREATE");
			$(".notificacao").html(response.responseJSON.message).fadeIn();
			setTimeout(function(){ $(".notificacao").fadeOut(); }, 2000);
		}
	});	

});

$("#loginAccount").click(function (){
	let data = {};
	$(this).html("Checking...");
	$(".login-form").serializeArray().forEach((input) => {
		data[input.name] = input.value;
	});

	$.ajax({
		url: "/user/authenticate",
		data: data,
		type: "POST", 
		success: function(response, status){
			$("#loginAccount").html("LOGIN");
			if(response.data.token){
				localStorage.setItem("tokenAcess", response.data.token);
				localStorage.setItem("nameUser", data.username);
				window.location = "/dashboard";
			}else{
				$(".notificacao").html("Erro ao fazer login!").fadeIn();
				setTimeout(function(){ $(".notificacao").fadeOut(); }, 2000);				
			}
		},
		error: function (response, status, error) {
			$("#loginAccount").html("LOGIN");
			$(".notificacao").html(response.responseJSON.message).fadeIn();
			setTimeout(function(){ $(".notificacao").fadeOut(); }, 1000);
		}
	});

});
