let ws = adonis.Ws()
			.withJwtToken(localStorage.getItem("tokenAcess"))
			.connect();

const postController = ws.subscribe('post');
const chatController = ws.subscribe('chat');
const friendController = ws.subscribe('friend');

let paginationRecentsPosts = 1;
let paginationRelevantsPosts = 1;
let paginationYourPosts = 1;

ws.on('open', () => {

	postController.on('listLastsPosts', (posts) => {
		$(".listRecentsPost").html("");
		if(posts.length == 0)
			$(".listRecentsPost").html("<span class='col-12 text-white text-center'>Nothing to show</span>");

		posts.forEach(function(post){
			showRecentPost(post);
		});

	});

	postController.on('listPostsMostRelevants', (posts) => {
		$(".listRelevantsPost").html("");
		if(posts.length == 0)
			$(".listRelevantsPost").html("<span class='col-12 text-white text-center'>Nothing to show</span>");

		posts.forEach(function(post){
			showRelevantsPost(post);
		});

	});

	postController.on('listYourPosts', (posts) => {
		$(".listYourPosts").html("");
		if(posts.length == 0)
			$(".listYourPosts").html("<span class='col-12 text-white text-center'>Nothing to show</span>");

		posts.forEach(function(post){
			showYourPost(post);
		});

	});

	friendController.on('listYourFriends', (friends) => {
		$(".listFriends").html("");
		if(friends.data.length == 0)
			$(".listFriends").html("<li class='nav-item'> <span class='nav-link'> you no have friends </span></li>");
		
		friends.data.forEach(function(friend){
			$(".listFriends").append("<li class='nav-item'> <span class='nav-link'> [friend name] </span></li>");
		});
	});

	postController.on('newPost', (post) => {
		showRecentPost(post, 'prepend');
	});

	postController.on('error', (response) => {
		toastr.error(response.message);
	});

	postController.emit('listLastsPosts', paginationRecentsPosts);
	postController.emit('listPostsMostRelevants', paginationRelevantsPosts);
	postController.emit('listYourPosts', paginationYourPosts);
	friendController.emit('listYourFriends', true);
	setInterval(function(){
		console.log('atualizar');
		postController.emit('listPostsMostRelevants', paginationRelevantsPosts);
	}, 60000);
})

ws.on('error', () => {
	console.log('error');
});


// START EVENTS

$(".btn.submitPost").click(function(){
	postController.emit('create', {
		text: $("textarea.textPost").val()
	});
	showYourPost({
		text: $("textarea.textPost").val(),
		created_at: moment().format('D/MM/YYYY h:mm:ss')
	}, 'prepend');	
});

$(".showPreviousRecentsPosts").click(function(){
	if(paginationRecentsPosts > 1){
		$(".listRecentsPost").html("");
		postController.emit('listLastsPosts', --paginationRecentsPosts);	
	}
});

$(".showNextRecentsPosts").click(function(){
	$(".listRecentsPost").html("");
	postController.emit('listLastsPosts', ++paginationRecentsPosts);
});

$(".showPreviousYourPosts").click(function(){
	if(paginationYourPosts > 1){
		$(".listYourPosts").html("");
		postController.emit('listYourPosts', --paginationYourPosts);	
	}
});

$(".showNextYourPosts").click(function(){
	$(".listYourPosts").html("");
	postController.emit('listYourPosts', ++paginationYourPosts);
});

$(".showPreviousRelevantsPosts").click(function(){
	if(paginationRelevantsPosts > 1){
		$(".listRelevantsPost").html("");
		postController.emit('listPostsMostRelevants', --paginationRelevantsPosts);	
	}
});

$(".showNextRelevantsPosts").click(function(){
	$(".listRelevantsPost").html("");
	postController.emit('listPostsMostRelevants', ++paginationRelevantsPosts);
});

// END EVENTS

// COMMON FUNCTIONS

function showRecentPost(post, order = 'append'){
	$(".templatePost .card-img-top").css("display", (post.attachment) ? 'block' : 'none');
	$(".templatePost .post").addClass("recent").removeClass("relevant");
	let templatePost = $(".templatePost").html();
	templatePost = templatePost.replace(/\[textPost\]/g, post.text);
	templatePost = templatePost.replace(/\[usuarioPostado\]/g, '@'+post.username);
	templatePost = templatePost.replace(/\[attachment\]/g, post.attachment);
	templatePost = templatePost.replace(/\[idPost\]/g, post.idPost);
	templatePost = templatePost.replace(/\[id_user\]/g, post.id_user);

	if(order == 'append') 
		$(".listRecentsPost").append(templatePost);
	else 
		$(".listRecentsPost").prepend(templatePost);

	if(post.rate == 'like')
		$(".post"+post.idPost+".recent .hatingArea").html('<div class="icon icon-shape icon-sm bg-success text-white rounded-circle shadow pointer"> <i class="fas fa-smile"></i> </div>');

	if(post.rate == 'deslike')
		$(".post"+post.idPost+".recent .hatingArea").html('<div class="icon icon-shape icon-sm bg-danger text-white rounded-circle shadow pointer"> <i class="fas fa-frown"></i> </div>');

	eventsPost(post.idPost)
}


function showRelevantsPost(post, order = 'append'){
	$(".templatePost .card-img-top").css("display", (post.attachment) ? 'block' : 'none');
	$(".templatePost .post").addClass("relevant").removeClass("recent");	
	let templatePost = $(".templatePost").html();
	templatePost = templatePost.replace(/\[textPost\]/g, post.text);
	templatePost = templatePost.replace(/\[usuarioPostado\]/g, '@'+post.username);
	templatePost = templatePost.replace(/\[attachment\]/g, post.attachment);
	templatePost = templatePost.replace(/\[idPost\]/g, post.idPost);
	templatePost = templatePost.replace(/\[id_user\]/g, post.id_user);

	if(order == 'append') 
		$(".listRelevantsPost").append(templatePost);
	else 
		$(".listRelevantsPost").prepend(templatePost);

	if(post.rate == 'like')
		$(".post"+post.idPost+".relevant .hatingArea").html('<div class="icon icon-shape icon-sm bg-success text-white rounded-circle shadow pointer"> <i class="fas fa-smile"></i> </div>');

	if(post.rate == 'deslike')
		$(".post"+post.idPost+".relevant .hatingArea").html('<div class="icon icon-shape icon-sm bg-danger text-white rounded-circle shadow pointer"> <i class="fas fa-frown"></i> </div>');

	eventsPost(post.idPost)
}

function showYourPost(post, order = 'append'){
	$(".templatePost .card-img-top").css("display", (post.attachment) ? 'block' : 'none');
	let templatePost = $(".templatePost").html();
	templatePost = templatePost.replace(/\[textPost\]/g, post.text);
	templatePost = templatePost.replace(/\[usuarioPostado\]/g, "Created at "+post.created_at);
	templatePost = templatePost.replace(/\[attachment\]/g, post.attachment);
	templatePost = templatePost.replace(/\[idPost\]/g, post.idPost);
	templatePost = templatePost.replace('hatingArea', 'hatingArea ocultar');
	if(order == 'append') 
		$(".listYourPosts").append(templatePost);
	else 
		$(".listYourPosts").prepend(templatePost);

}

function eventsPost(idPost){
	$(".post"+idPost+" .likePost").on('click', function(){
		$.ajax({
			url: "/rate/post",
			headers: { "Authorization": "Bearer " + localStorage.getItem("tokenAcess") },
			data: {idPost: idPost, rate: 'like'},
			type: "POST", 
			success: function(response, status){
				$(".post"+idPost+" .hatingArea").html('<div class="icon icon-shape icon-sm bg-success text-white rounded-circle shadow pointer"> <i class="fas fa-smile"></i> </div>');
			},
			error: function(response){
				toastr.error(response.responseJSON.message);
			}
		});
	});

	$(".post"+idPost+" .deslikePost").on('click', function(){
		$.ajax({
			url: "/rate/post",
			headers: { "Authorization": "Bearer " + localStorage.getItem("tokenAcess") },
			data: {idPost: idPost, rate: 'deslike'},
			type: "POST", 
			success: function(response, status){
				$(".post"+idPost+" .hatingArea").html('<div class="icon icon-shape icon-sm bg-danger text-white rounded-circle shadow pointer"> <i class="fas fa-frown"></i> </div>');
			},
			error: function(response){
				toastr.error(response.responseJSON.message);
			}
		});
	});

	$(".post"+idPost+" .messageAddFriend").on('click', function(){
		// $(this).attr('id')
		// friendController.emit('addNewFriend', {
		// 	message: '',
		// 	id_user_to: ''
		// });
	});

}


$('.templateChat header').on('click', function() {

	$('.chat').slideToggle(300, 'swing');
	$('.chat-message-counter').fadeToggle(300, 'swing');

});

$('.chat-close').on('click', function(e) {

	e.preventDefault();
	$('.templateChat').fadeOut(300);

});