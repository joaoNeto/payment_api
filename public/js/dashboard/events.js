if(localStorage.getItem("tokenAcess") == null){
	window.location = "/";
}

$(".nomeUsuario").html("@"+localStorage.getItem("nameUser"));
$("title").html("Crow - "+localStorage.getItem("nameUser"));


// START EVENTS
$(".logout").click(function(){
	localStorage.removeItem("tokenAcess");
	window.location = "/";	
});

$(".openModalProfile").click(function(){
	$("#modalChangeProfile input[name=password]").val("");
	$.ajax({
		url: "/user",
		headers: { "Authorization": "Bearer " + localStorage.getItem("tokenAcess") },
		type: "GET", 
		success: function(response, status){
			$("#modalChangeProfile input[name=username]").val(response.data.username);
			$("#modalChangeProfile input[name=email]").val(response.data.email);
		}
	});
});

$(".updateUserData").click(function(){
	let data = {};
	$(".update-form").serializeArray().forEach((input) => { data[input.name] = input.value; });

	$.ajax({
		url: "/user",
		headers: { "Authorization": "Bearer " + localStorage.getItem("tokenAcess") },
		data: data,
		type: "PUT", 
		success: function(response, status){
			localStorage.setItem("nameUser", $("#modalChangeProfile input[name=username]").val());
			$(".nomeUsuario").html("@"+localStorage.getItem("nameUser"));
			$("title").html("Crow - "+localStorage.getItem("nameUser"));
		    $('.closeModelChangeUser').click();
			toastr.error("User was change");
		},
		error: function(response){
			toastr.error(response.responseJSON.message);
		}
	});
});

$(".deleteProfile").click(function(){
	
	if(confirm("you really want to delete your account?")){
		$.ajax({
			url: "/user",
			headers: {
				"Authorization": "Bearer " + localStorage.getItem("tokenAcess")
			},
  			type: "DELETE", 
			success: function(response, status){
				localStorage.removeItem("tokenAcess");
				window.location = "/";	
			},
			error: function (response, status, error) {
				let errorMessage = "Error to delete user";
				switch(response.status) {
				  case 401:
				    	errorMessage = "User not authenticate to delete account!";
				    break;
				  case 400:
				    	errorMessage = response.responseJSON.message;
				    break;
				  case 500:
				    	errorMessage = "Internal error, contact the suport for more details!";
				    break;
				}
				toastr.error(errorMessage);
			}
		});
	}

});

// END EVENTS
