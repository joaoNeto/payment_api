## WELCOME TO PAYMENT

### What is it?
The payment is a generic REST API for online payments, with the payment you can integrate with differents types of payments system how <a href="https://pagar.me/">pagarme</a> or <a href="https://pagseguro.uol.com.br/">pagseguro</a>.  

### How does it work?

### Instalation

#### System requirements?
For you install the payment on your server make sure that the <a href="https://nodejs.org/en/">node.js</a> has been installed.

#### Installing payment
For run the payment certify that you have the <a href="https://adonisjs.com/">adonis.js</a>, if you don't have execute this command in your terminal.

> npm i -g @adonisjs/cli

After go to your path project use those commands.

to install all the dependencies
> npm install

to install the databases

> adonis migration:run

and to run the project

> adonis serve --dev

and to run the tests

> adonis test

