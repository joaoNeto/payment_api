'use strict'

const Ws = use('Ws')
Ws.channel('chat', 'ChatController').middleware(['auth'])
Ws.channel('friend', 'FriendController').middleware(['auth'])
Ws.channel('post', 'PostController').middleware(['auth'])