'use strict'
const Route = use('Route')

// VIEW
Route.on('/').render('login')
Route.on('/dashboard').render('dashboard')
Route.on('/chat').render('chat')

// API
Route.get('/user', "UserController.getDataUser").middleware(['auth']);
Route.post('/user', "UserController.create").validator('User');
Route.post('/user/authenticate', "UserController.authenticate");
Route.put('/user', "UserController.update").middleware(['auth']);
Route.delete('/user', "UserController.delete").middleware(['auth']);

Route.post('/rate/post', "RatePostController.create").middleware(['auth']);
