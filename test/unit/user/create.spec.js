'use strict'

const { test, trait } = use('Test/Suite')('Create user');
trait('Test/ApiClient')

test('Create a simple user', async ({ assert }) => {
	assert.isTrue(true)
});

test('Create a simple user and get success api response', async ({ assert }) => {
	assert.isTrue(true)
});

test('Create two equals users will generate a error', async ({ assert }) => {
	assert.isTrue(true)
});

test('Create two equals users will generate a error and get error api response', async ({ assert }) => {
	assert.isTrue(true)
});

test('Create a user with sql injection', async ({ assert }) => {
	assert.isTrue(true)
});

test('Create a user with a wrong email will generate a error', async ({ assert }) => {
	assert.isTrue(true)
});

test('Create a user without a email will generate a error', async ({ assert }) => {
	assert.isTrue(true)
});

test('Create a user without a password will generate a error', async ({ assert }) => {
	assert.isTrue(true)
});

test('Create a user without a username will generate a error', async ({ assert }) => {
	assert.isTrue(true)
});
