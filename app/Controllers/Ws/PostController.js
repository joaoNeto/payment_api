'use strict'
const postModel = use('App/Models/Post')
const Database  = use('Database')

class PostController {

  constructor ({ socket, request, auth }) {
    this.socket  = socket
    this.request = request
    this.auth 	 = auth
  }

  /**
   * Create a post
   * @param {String} text texto do post
   * @param {File} attachment arquivo de imagem
   */
  async onCreate (data) {
  	try{
		await postModel.create({
			id_user: this.auth.user.id,
			text: data.text,
			attachment: data.attachment
		});

	  	data.username = this.auth.user.username;
	  	data.idUser = this.auth.user.id;
	    this.socket.broadcastToAll('newPost', data)
  	}catch(err){
		this.socket.emitTo('error', {message: "Erro ao gerar post", details: err}, [this.socket.id]);
  	}
  }

  async onListLastsPosts(pagina = 1){
    let listPosts = await Database
      .table('post')
      .select('post.text', 'post.attachment', 'user.username', 'user.id as id_user', 'post.id as idPost', 'ratepost.rate',  Database.raw('DATE_FORMAT(post.created_at, "%d/%m/%Y %h:%i:%s") as created_at'))
      .innerJoin('user', 'user.id', 'post.id_user')
      .leftJoin('ratepost', 'ratepost.id_post', 'post.id')
      .orderBy('post.id', 'desc')
      .groupBy('post.id')      
      .forPage(pagina, 8);
      // .on('query', console.log);

    this.socket.emitTo('listLastsPosts', listPosts, [this.socket.id])
  }

  async onListPostsMostRelevants(pagina = 1){
    let listPosts = await Database
      .table('post')
      .select(
        'post.text', 
        'post.attachment', 
        'user.username',
        'user.id as id_user',
        'post.id as idPost',
        'ratepost.rate',
        Database.raw("(select count(*) from ratepost rp where rp.id_post = post.id and rp.rate = 'like') - (select count(*) from ratepost rp where rp.id_post = post.id and rp.rate = 'deslike')  AS ratePost"))
      .innerJoin('user', 'user.id', 'post.id_user')
      .leftJoin('ratepost', 'ratepost.id_post', 'post.id')
      .orderBy('ratePost', 'desc')
      .groupBy('post.id')
      .forPage(pagina, 8);

    this.socket.emitTo('listPostsMostRelevants', listPosts, [this.socket.id])
  }
 
  async onListYourPosts(pagina = 1){
    let listPosts = await Database
      .table('post')
      .select('text', 'attachment', 'id as idPost', Database.raw('DATE_FORMAT(created_at, "%d/%m/%Y %h:%i:%s") as created_at'))
      .where('id_user', this.auth.user.id)
      .orderBy('id', 'desc')
      .forPage(pagina, 8);

    this.socket.emitTo('listYourPosts', listPosts, [this.socket.id])
  }

}

module.exports = PostController
