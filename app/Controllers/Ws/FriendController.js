'use strict'

class FriendController {

  constructor ({ socket, request, auth }) {
    this.socket  = socket
    this.request = request
    this.auth 	 = auth
  }

  async onCreate (data) {
  	try{

  	}catch(err){
		this.socket.emitTo('error', {message: "Erro ao gerar post", details: err}, [this.socket.id]);
  	}
  }

  async onListYourFriends (data) {
	 this.socket.emitTo('listYourFriends', { message: "List your friends", data: [] }, [this.socket.id]);
  }

}

module.exports = FriendController
