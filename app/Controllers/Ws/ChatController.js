'use strict'

class ChatController {
  constructor ({ socket, request }) {
    this.socket = socket
    this.request = request

/*

this.socket.id

socket.on('close', () => {
});

socket.on('error', () => {
});


socket.emitTo('greeting', 'hello', [someIds])

*/

  }

  onMessage (message) {
    this.socket.broadcastToAll('message', message)
  	// this.socket.emit('message', 'Hello world')

  }
  
}

module.exports = ChatController
