'use strict'
const rateModel = use('App/Models/RatePost')

class RatePostController {

    async create({request, response, auth}){
        try{
			const {idPost, rate} = request.all();

            await rateModel.create({
            	id_user: auth.user.id,
            	id_post: parseInt(idPost),
            	rate: rate
            });

            response.status(201).send({ message: "rated with sucess!" })
        }catch(err){
            response.status(400).send({message: "error to rate post", response: err.message})
        }
    }

}

module.exports = RatePostController
