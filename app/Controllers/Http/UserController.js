'use strict'

const userModel = use('App/Models/user')
const { validate } = use('Validator')

class UserController {

    async create({request, response, auth}){
        try{
            const user = request.only([
                'username',
                'email',
                'password'
            ]);
            await userModel.create(user);
            const acesso = await auth.attempt(user.username, user.password);
            response.status(201).send({
                message: "user was created", 
                data: acesso
            })
        }catch(err){
            response.status(400).send({message: "error creating user", response: err.message})
        }
    }

    async authenticate({request, response, auth}){
        try{
            const {username, password} = request.all();
            const acesso = await auth.attempt(username, password);
            response.status(200).send({
                message: "User was found", 
                data: acesso
            });
        }catch(err){
            response.status(400).send({message: "error found user", response: err.message})
        }
    }

    async getDataUser({response, auth}){
        response.status(200).send({
            message: "User data", 
            data: auth.user
        });
    }

    async update({request, response, auth}){
        try{
            const {username, email, password} = request.all();
            let user = await userModel.findOrFail(auth.user.id);
            
            if(username)
                user.username = username;
            if(email)
                user.email = email;
            if(password)
                user.password = password;

            await user.save();
            response.status(201).send({ message: "User was change" });
        }catch(err){
            response.status(400).send({ message: "Fail to change user data" });
        }
    }

    async delete({request, response, auth}){
        try{
            const deleted = await userModel.query().where('id', auth.user.id).delete();
            if(deleted){
                response.status(201).send({
                    message: "User was deleted"
                });                
            } else {
                throw "Can't delete user";
            }
        }catch(err){
            response.status(400).send({message: "Fail to delete user"});
        }
    }

}

module.exports = UserController
