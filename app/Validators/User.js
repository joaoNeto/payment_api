'use strict'

class User {

  get rules () {
    return {
      email: 'required|email|min:4|unique:User',
      username: 'required|min:4|unique:User',
      password: 'required|min:4'
    }
  }

  get messages () {
    return {
      'email.required': 'Voce precisa informar o email',
      'email.email': 'Voce precisa informar um email valido',
      'email.unique': 'Este email já existe',
      'password.required': 'Voce precisa informar a senha'
    }
  }

  async fails(errors) {
    return this.ctx.response.status(403).json({
      message: 'Erro ao validar os dados do usuário, '+errors[0].message
    })
  }

  get validateAll () {
    return true
  }

}

module.exports = User