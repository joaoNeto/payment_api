'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class RatePost extends Model {

  static get table() {
    return 'ratePost'
  }

  static boot () {
    super.boot()
  }


}

module.exports = RatePost
