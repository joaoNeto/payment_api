'use strict'

const Model = use('Model')

class Post extends Model {

  static get table() {
    return 'post'
  }
  static get primaryKey() {
    return 'id'
  }
  static get hidden () {
    return []
  }

  static boot () {
    super.boot()
  }

}

module.exports = Post
