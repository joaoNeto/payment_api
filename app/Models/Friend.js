'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Friend extends Model {

  static get table() {
    return 'friend'
  }

}

module.exports = Friend
